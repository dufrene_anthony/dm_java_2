import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b)
    {
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste)
    {
        Integer mini = null;
        if (liste.size() != 0)
        {
          mini = liste.get(0);
          for (Integer i : liste)
          {
            if (mini > i)
            {
              mini = i;
            }
          }
        }
        return mini;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        if (liste.isEmpty())
        {
            return true;
        }
        else
        {
            for (T elem : liste)
            {
                if (valeur.compareTo(elem)>=0)
                {
                    return false;
                }
            }
            return true;
        }
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        Set<T> set1 = new HashSet<>(liste1);
        Set<T> set2 = new HashSet<>(liste2);
        set2.retainAll(set1);
        List<T> intersect = new ArrayList<>(set2);
        Collections.sort(intersect);
        return intersect;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte)
    {
        List<String> liste = new ArrayList<>();
        if (texte.length()==0)
        {
            return liste;
        }

        String texte2 = new String(texte.replaceAll("\\s+", " "));
        if (texte2.charAt(0)==' ')
        {
            String newtexte2 = texte2.substring(1, texte2.length()-1);
            return Arrays.asList(newtexte2.split(" "));
        }
        return Arrays.asList(texte2.split(" "));
    }



    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte)
    {
        if (texte.length()==0)
        {
            return null;
        }
        List<String> liste = new ArrayList<>();
        liste = BibDM.decoupe(texte);
        int maximum = Collections.frequency(liste, liste.get(0));
        String str = liste.get(0);
        for (int i =1;i<liste.size()-1;i++){
          if (Collections.frequency(liste, liste.get(i))>=maximum)
          {
                str = liste.get(i);
            }
          }
        return str;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */

     public static boolean bienParenthesee(String chaine){
         int cpt1 = 0;
         int cpt2 = 0;
         boolean res = true;
         for (int i=0; i<chaine.length();i++){
             if (chaine.charAt(i) == '(' ){cpt1+=1;}
             else if (chaine.charAt(i) == ')' ){cpt2+=1;}

             if (cpt2 > cpt1){res = false;}
         }
         if (cpt2 != cpt1){res = false;}
         return res;
     }


    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        List<Character> lstOpen = new ArrayList<>();
        char charI;

        for(int i = 0; i < chaine.length(); i++)
        {
            charI = chaine.charAt(i);
            if(charI == '(' || charI == '[')
                lstOpen.add(charI);

            else if(charI == ']')
            {
                if(lstOpen.size() == 0 || !lstOpen.get(lstOpen.size() - 1).equals('['))
                    return false;
                else
                    lstOpen.remove(lstOpen.size() - 1);
            }

            else if(charI == ')')
            {
                if(lstOpen.size() == 0 || !lstOpen.get(lstOpen.size() - 1).equals('('))
                    return false;
                else
                    lstOpen.remove(lstOpen.size() - 1);
            }
        }
        return lstOpen.size() == 0;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        int inddeb = 0;
        int indfin = liste.size()-1;
        int indmilieu;
        boolean trouve = false;
        while (!trouve && (inddeb <= indfin)){
          indmilieu = (inddeb + indfin) / 2;
          if (valeur > liste.get(indmilieu)){
            inddeb = indmilieu + 1;
          }
          else if (valeur == liste.get(indmilieu)){
            trouve = true;
          }
          else {
            indfin = indmilieu - 1;
          }
        }
        return trouve;
    }
}
